(function( $ ) {
  $.fn.ezslider = function(options) {

/*
Author : William R. Thornton

Contact : willofthewild@gmail.com 

Website : TimeTravelingNachos.com / .org

License : Free non-commercial use. For commercial use please contact me for approval. I do not ask for
payment but please consider donating a few dollars via paypal if my plugin helps make your life easier.
My paypal address is the same as my contact email for reference. No guarantees are made on this code, 
it will work with multiple nested ULs in LIs so have fun.

Plugin Explination :
To use this plugin simply call $().ezslider(); on an element containing a UL. The plugin will take all li's from
said div containing a UL and turn them into styled panes for viewing.  The background of the panes, the 
slider itself, and the item counter colors can be changed easily shown below. The default installation of this 
plugin requires NO ADDED CSS. All css is generated with jQuery and named in such a way as to avoid collision.  

PLUGIN OPTIONS EXPLAINED
Direction : Default 'h' , use 'v' for a vertical scroll, arrows update automatically if using my provided png.

Background : Default 'black', any css value will work.

Content background : Defaults to lightblue, any css value will work.

Counter color : Changes the color of the X/X counter. Defaults to white, any css value will work. 

Pane width : Sets the width of the viewing area prepadding.

Pane height : Sets the height of the viewing area prepadding.

Pane Padding : Sets the padding of the pane contents.

Pane Scroll : Sets whether or not you want the panes to scroll vertically. Set to 'scroll' by default, can be 
set to off with 'none'.

Automatic : Sets whether or not the slider scrolls on its own. Defauls 'off', set to 'on' to enable.

Timer : If automatic setting is 'on' you can set the timer interval. Defaults to 10 for 10 seconds. No need for milliseconds.

Links : Sets whether or not to display paginated links. Defaults to 'off'.

Link Color : Sets another color for the pagination links. Defaults to 'lime'.

Active Link Color : Sets the active pagination link color when clicked. Defaults to 'orange'.
*/

// Append all necessary elements to parent element to save on div inclusions per use case

var ezslider = $(this);
//DEFAULT OPTIONS!//
    var settings = $.extend( {
	  'direction':'h',
	  'background' : 'black',
	  'contentbackground' : 'lightblue',
	  'counter':'on',
	  'countercolor' : 'white',
	  'panewidth':250,
	  'paneheight':250,
	  'panepadding':6,
	  'panescroll':'scroll',
	  'automatic': 'off',
	  'timer': 10,
	  'links': 'off',
	  'linkcolor' : 'lime',
	  'activelinkcolor':'orange',
	}, options);

    return this.each(function() {  

// PLUGIN START! //

// ----------------------------------------------------------------------------------------------//	
	var specifihandler = ((Math.random()*10)+1); 
	var derphandler = Math.ceil(specifihandler);
	var specifichandler = derphandler;
	var linkpos = 1;
// HTML WRAPS, APPENDS, FINDS, ETC //
	
// Find the UL in the div the plugin is called on and wrap the slider around it.
// :not(ul li > ul) to specify the outter most UL, not the inner UL to avoid recursion.
	$(this).find('ul:not(ul li > ul)').wrap('<div class="ezslider-'+specifichandler+'"/>').addClass('slidercontent');

//Style all the li's as panes based on the plugin settings or using defauls.
//:not(ul li ul > li) to specify only parent level LI.
	$(this).find('li:not(ul li ul > li)').css({
	'float' : 'left',
	'background':settings.contentbackground,
	'width' : settings.panewidth,
	'padding' : settings.panepadding,
	'height' : settings.paneheight,
	});


//Wrap the slider in a viewing pane with class ezslider-11 to avoid css naming conflicts
	$('.ezslider-'+specifichandler).wrap('<div class="sliderview"/>');

//Links for 'pagination'
function buildcontrols(ezslider,links,count,counter){
	
	
	switch (links){
	case 'on' :
	ezslider.append("<div class='controls'><a href='#' class='left'></a><a href='#' class='right'></a><span class='eol'/></div>");

	var liltemp = 0;
	while( liltemp < count){
	liltemp++;
	ezslider.find('.eol').before('<a href="#" data-linknum="'+liltemp+'" class="tabno">'+liltemp+'</a>');
	}
ezslider.find('.eol').after('<br/>');
ezslider.find('.tabno').css({   
   'background': settings.linkcolor,
    'border-radius': '20%',
    'margin-left': '.2em',
    'color': 'black',
    'text-decoration': 'none',
	});
	break;
	case 'off' :
	ezslider.append("<div class='controls'><a href='#' class='left'></a><a href='#' class='right'></a></div>");
	break;

}

switch(counter){
	case 'on' : 
	ezslider.find('.controls').append('<span class="number"></span>');
	break;
	case 'off' :
	break;
	}
	
	
	
	
};


//Make variables for default slider */* count, not child elements of nested lists
	var count = $(this).find('li:not(ul li ul > li)').length;
buildcontrols(ezslider,settings.links,count,settings.counter);
// what script is complete without i=0 ammirite?
	var i=0;
//alert($('a .tabno').size());
$(this).find('.tabno').click(function(){
ezslider.find('.activepaginationlink').each(function(){
$(this).removeClass('activepaginationlink');
	$(this).css({
	'background':settings.linkcolor,
	});
});
$(this).addClass('activepaginationlink');
	//Active Pagination Link CSS
	ezslider.find('.activepaginationlink').css({
	'background':settings.activelinkcolor,
	});
var tabrequest = $(this).data('linknum');
var currentpos = (i+1);

//BACKWARDS
if(currentpos<tabrequest){
var offset = (tabrequest-currentpos);
i = (i+offset);
move(offset, 'forwards');
ezslider.find('.number').html((i+1)+'/'+count);
}else if(currentpos>tabrequest){
//FORWARDS
var offset = (currentpos-tabrequest);
i = (i-offset);
move(offset, 'backwards');
ezslider.find('.number').html((i+1)+'/'+count);
}
});
//Set the */* in the slider default, without this it falls back on the Left/Right class link clicks to appear.
	$(this).find('.controls .number').html((i+1)+'/'+count);

// END HTML ADDITIONS //

// ---------------------------------------------------------------------------------------------//

// STYLES //


//Sets the width of the element holding the panes in a clip, do not edit this, it is dynamic based on plugin options.
	var slidewidth = (settings.panewidth+(settings.panepadding*2));
	var slideheight = (settings.paneheight+(settings.panepadding*2));
	var totalsliderwidth = (count*settings.panewidth+((settings.panepadding*2)*count));
	var totalsliderheight = (count*settings.paneheight+((settings.panepadding*2)*count));
	var loopheight = ((count-1)*settings.paneheight+((settings.panepadding*2)*(count-1)))
	var loopwidth = ((count-1)*settings.panewidth+((settings.panepadding*2)*(count-1)))
	var actualtime = (settings.timer * 1000);
	//Style the div itself and making sure overflow is hidden.
	$(this).css({
	'padding':'2px',
	'background':settings.background,
	'border-radius':'10px',
	'display': 'inline-block',
    'overflow': 'hidden',
	});
	
	//Sets the styles for the viewing pane
	$(this).find('.sliderview').css({
	'width':slidewidth,
	'height':slideheight,
	'display':'inline-block',
	'overflow':'hidden',
	'border':'2px solid black',
	'border-radius':'8px',
	});
	
	//Styles the UL itself removing padding and margins and enables it to slide by changing the position to relative

	//Styles for Horizontal sliding
	if(settings.direction == 'h'){
	$(this).find('.slidercontent').css({
    'width': totalsliderwidth,
    'margin': '0',
    'padding': '0',
	'position':'relative',
	}); 
	
	//Styles for veritcal scrolling
	}else if (settings.direction == 'v'){
		$(this).find('.slidercontent').css({
    'width': settings.panewidth,
	'height':totalsliderwidth,
    'margin': '0',
    'padding': '0',
	'position':'relative',
	});
	}
	
	//Style the slider so it moves and is big enough to hold all the panes
	
	//Styles for horizontal 
	if(settings.direction == 'h'){
	$(this).find('.ezslider-'+specifichandler).css({
	'width' : totalsliderwidth,
	'position' : 'relative',
	});//Styles for veritcal
	}else if(settings.direction == 'v'){
	$(this).find('.ezslider-'+specifichandler).css({
	'width' : settings.panewidth,
	'position' : 'relative',
	});
	}
	

	//Sets styles on content panes
	$(this).find('.slidercontent li:not(ul li ul > li)').css({
	'vertical-align':'top',
	'display':'inline-block',
	'margin':'0',
	'height':settings.paneheight,
	'overflow-y':settings.panescroll,
	});

	
	//Align */* counter in center of slider
	$(this).find('.controls').css({
	'text-align':'center',
	});

	
	//Colors the */* counter
	$(this).find('.controls .number').css({
	'color':settings.countercolor,	
	});
	
	
//Styles for horizontal arrows
	if(settings.direction == 'h'){
	//Floats the right arrow to the right, sets height, width, background
	$(this).find('.controls .right').css({
	'float':'right',
	'width':'28px',
	'height':'32px',
	'background':'url(imgs/arrows.png)96px, 0px',
	});

	
	//Floats the left arrow to the left, sets height, width, background
	$(this).find('.controls .left').css({
	'float':'left',
	'width':'28px',
	'height':'32px',
	'background':'url(imgs/arrows.png)0px, 0px',
	}); 
	// Styles for vertical arrows
	}else if(settings.direction == 'v'){	
	//Floats the right arrow to the right, sets height, width, background
	$(this).find('.controls .right').css({
	'float':'right',
	'width':'28px',
	'height':'32px',
	'background':'url(imgs/arrows.png) 34px, 0px',
	});

	//Floats the left arrow to the left, sets height, width, background
	$(this).find('.controls .left').css({
	'float':'left',
	'width':'28px',
	'height':'32px',
	'background':'url(imgs/arrows.png)64px, 0px',
	});
	}
	

// END STYLES //

//--------------------------------------------------------------------------//

// BUTTON LOGIC / MOVEMENT CODE //

//removes active button class and resets css
function clearbuttons(){
ezslider.find('.activepaginationlink').each(function(){
	ezslider.removeClass('activepaginationlink');	
	ezslider.find('a.tabno').css({
	'background':settings.linkcolor,
	});
	});
	}	
	


if(settings.automatic=='on'){
updatelinkcolorsauto();
setInterval(function(){ 
	var count = $('.ezslider-'+specifichandler+' .slidercontent li:not(ul li ul > li)').length;
	var gr = (count-i);
	//If there isn't a pane to the right, stop.
	if(gr>1){
	i++;
	move(1,'forwards');
	ezslider.find('.controls .number').html((i+1)+'/'+count);
	clearbuttons();
	//Why 3 you might ask? i=0 above, 1 for each control, boom. 3. 
	ezslider.find(".tabno:nth-child("+(i+3)+")").addClass('activepaginationlink');
	//Active Pagination Link CSS
	ezslider.find(".tabno:nth-child("+(i+3)+")").css({
	'background':settings.activelinkcolor,
	});
	}else if (gr==1){
	if(settings.direction == 'h'){
	ezslider.find('.ezslider-'+specifichandler).animate({'left' : '+='+loopwidth}, 400);
	i=0;
	ezslider.find('.controls .number').html((i+1)+'/'+count);
	clearbuttons();
	ezslider.find('.tabno:nth-child(3)').css({
	'background':settings.activelinkcolor,
	});
	}else if (settings.direction == 'v'){
	ezslider.find('.ezslider-'+specifichandler).animate({'top' : '+='+loopheight}, 400);
	i=0;
	ezslider.find('.controls .number').html((i+1)+'/'+count);
	clearbuttons();
	ezslider.find('.tabno:nth-child(3)').css({
	'background':settings.activelinkcolor,
	});
	}
	}

}, actualtime);

}

// Button highlight logic
function updatelinkcolorsauto(){
	if(settings.links=='on'){
	ezslider.find('.activepaginationlink').each(function(){
	$(this).removeClass('activepaginationlink');	
	$(this).css({
	'background':settings.linkcolor,
	});
	});
	//Why 3 you might ask? i=0 above, 1 for each control, boom. 3. 
	ezslider.find(".tabno:nth-child("+(i+3)+")").addClass('activepaginationlink');
	//Active Pagination Link CSS
	ezslider.find(".tabno:nth-child("+(i+3)+")").css({
	'background':settings.activelinkcolor,
	});
	};
}



// Navigation Buttons : LEFT (ACTUAL MOVEMENT CODE)

	function move(distance, direct){
	
	if(settings.direction == 'h'){
	var totaldistance = (distance*slidewidth) ;
	}else if(settings.direction == 'v'){
	var totaldistance = (distance*slideheight) ;
	}
	
	//alert(distance);
	switch(direct){
	case 'backwards':
	if(settings.direction == 'h'){
	$('.ezslider-'+specifichandler).animate({'left' : '+='+totaldistance}, 400);
	}else if (settings.direction == 'v'){
	$('.ezslider-'+specifichandler).animate({'top' : '+='+totaldistance}, 400);
	}	
	break;

	case 'forwards' : 
	if(settings.direction == 'h'){
	$('.ezslider-'+specifichandler).animate({'left' : '+=-'+totaldistance}, 400);
	}else if (settings.direction == 'v'){
	$('.ezslider-'+specifichandler).animate({'top' : '+=-'+totaldistance}, 400);
	}
	break ;
	}
	
	}


// Navigation Buttons : RIGHT (ACTUAL MOVEMENT CODE)



// Navigation button on click logic - LEFT
	$(this).find('a.left').click(function(event){
	event.preventDefault();
	var count = ezslider.find('li:not(ul li ul > li)').length;
	var gr = (count-i);
	//If there isn't a pane to the left, stop. 
	if(gr<count){
	i--;
	move(1,'backwards');
	updatelinkcolorsauto();
	ezslider.find('.controls .number').html((i+1)+'/'+count);
	};
	});

// Navigation button on click logic - RIGHT	
$(this).find('a.right').click(function(event){
	var count = ezslider.find('li:not(ul li ul > li)').length;
	var gr = (count-i);
	event.preventDefault();
	//If there isn't a pane to the right, stop.
	if(gr>1){
	i++;

	move(1,'forwards');
	updatelinkcolorsauto();
	ezslider.find('.controls .number').html((i+1)+'/'+count);
	}
	});

// END MOVEMENT CODE //

// ----------------------------------------------------------------------------------//

});
}
})( jQuery );