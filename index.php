
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.js"></script>
<script src="jquizz.js"></script>
<link rel='stylesheet' type='text/css' href='style.css'/>
</head>
<body>

<div id="bigdiv">

<div class="socialme">

<ul>
<li>

<h3> Will's new content slider!</h3>
<p>Well how about that, I wrote my first useful plugin in jQuery!</br>To see how simple it is, click next.</p>
</li>
<li>
<h3>Step 1</h3>
<p>Download the free zip file included <a href='#'>here</a> and put it in your testing directory. There should be (3) files and (2) folders listed as </br></br>
/slider/(folder)<br/>
/slider/index.php<br/>
/slider/ezslider.js</br>
/slider/imgs/ (folder)</br>
/slider/imgs/arrows.png</br>
</p>
</li>
<li>
<h3> Step 2</h3>
<p>Add jQuery files and CSS to header of page as usual. I personally use Google for hosting, why not right?</br></br>
Here are the links I used for my development<br/><br/>
src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"
src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.js"
</p>
<ul>

<li>Nested UL/LI test.. 
<ul>
<li>hey 1</li>
<li>hey 2</li>
<li>hey 3</li>
<li>hey 
<ul>
<li>hey 1</li>
<li>hey 2</li>
<li>hey 3</li>
<li>hey 
<ul>
<li>hey 1</li>
<li>hey 2</li>
<li>hey 3</li>
</ul>
</li>
</ul>
</li>
</ul>
</li>

</ul>
</li>
<li>

<h3> Step 3</h3>
<p>Create a div on your page.</p>
</li>
<li>
<h3> Step 4</h3>
<p>Create an Unordered List (UL) in the div you just created. Each list item's contents will be interpretted as a panel so keep that in mind.<br/></br> 
</li>
<li>
<h3> Step 5</h3>
<p>Call the ezslider plugin with the following code for most basic initialization<br/><br/>
$(div or class).ezslider();
<br/>
<br/>
Continue to options
</li>
<li>
<h3> Options </h3>
<p>You can change the following with ease;</p>
<ul>
<li>Direction of the slider (horizontal or vertical)</li>
<li>Slider-pane background color</li>
<li>The slider frame color</li>
<li>The X/X counter color</li>
<li>The height/width of the slider panels</li>
<li>Edit padding on the panels</li>
<li>Timer for automatic slide</li>
<li>And remove the scroll bar if necessary without editing the JS itself.</li>

</ul>
</br>Click next for a preview example code of a fully altered slider.<br/><br/>

</li>
<li>
<h3>Extended Example Code</h3>
<p>This is an example of the full 'bells and whistles' mode. Every possible stock option changed for reference.</p>
<p>
$('div').ezslider({<br/>
	  'direction' : 'v', //defaults to horizontal <br/>
	  'background' : 'black',   // This can also be an image if you use 'url(path.extension)'
	  <br/> 
	  'contentbackground' : 'lightblue',<br/>
	  'counter':'off', // defaults on
	  'countercolor' : 'white',<br/>
	  'automatic' : 'on', // defaults to off, turn 'on' for a slideshow with a 10 second default timer
	  'timer' : 20, //defaults to 10, number of seconds for each slide to show
	  'links' : 'on', // defaults to 'off', turn 'on' for paginated links.
	  'linkcolor' : 'orange', // defaults to 'lime', colors paginated circular links.
	  'activelinkcolor' : 'lightblue', //defaults to 'orange', colors selected pagination link.
	  'panewidth':250,<br/>
	  'paneheight':250,<br/>
	  'panepadding':6,<br/>
	  'panescroll':'scroll',<br/>
	  'scroll':'none',<br/>
});<br/>
</p>
</li>
<li>
<h3>Why ezslider?</h3>
<p>There are a few reasons this plugin might be for you : <br/><br/>
Requries NO ADDED CSS, all styles are added with jQuery.css on the fly and is easily editable.<br/><br/>
Requires NO ADDED ELEMENTS beyond the initial element to instansiate the plugin on. All other elements are applied on the fly with .wrap, append, before/after, etc.<br/><br/>
Dynamically resizes hidden panels so 10 LI or 10,000 it works and doesn't over scroll.<br/><br/>
Multiple sliders per page with no conflicts!<br/><br/>
Absolutely FREE! (except I do take donations, please...)<br/><br/>
</p>
</li>
</ul>


</div>

<script>
$('.socialme').ezslider({
'panewidth':500,
'direction':'h',
'counter':'on',
});




</script>
</div>
</body>
</html>